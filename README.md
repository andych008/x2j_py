# 安卓to鸿蒙:资源迁移工具

#### 介绍
安卓项目向鸿蒙移植的过程中，大家肯定会批量修改资源文件。主要涉及string color dimen theme 以及 drawable。

安卓的string color dimen theme是定义在xml文件中，鸿蒙则是用定义在json文件中。

目前**还不支持**安卓的`drawable`转成鸿蒙的`graphic`，它们都定义在xml中，不过也要做相应的转换。对python熟悉的小伙伴可以和我一起完善。

#### 构建
- 要求Python 3.6+

- 依赖：
    ```shell
    #在工程根目录执行
    python3 -m venv .venv
    source .venv/bin/activate
    pip install xmltodict fastapi uvicorn python-multipart
    ```
    
- 运行：
    ```shell
    python3 web.py
    ```
    
- 浏览器打开:http://127.0.0.1:8000

    ![pic_01](static/pic_01.jpg)

#### 技术方案
1. 参考1:[使用 python fastapi+vue 快速搭建网站](https://www.elprup.com/2020/09/19/fastapi_vue/)
2. 参考2：[MigrateToAndroidX](https://github.com/yuweiguocn/MigrateToAndroidX )
3. 参考3: https://github.com/abdolence/x2js

#### 没有python环境，拿来直接用
https://x2j.unclecat.wang/

